package lx16a

import (
	"errors"
	"fmt"
)

type ServoMode uint8
type ServoError int

const (
	PositionMode ServoMode = 0
	MotorMode    ServoMode = 1
)

const (
	NO_ALARM                        ServoError = 0
	OVER_TEMP                       ServoError = 1
	OVER_VOLT                       ServoError = 2
	OVER_TEMP_AND_OVER_VOLT         ServoError = 3
	LOCKED_ROTOR                    ServoError = 4
	OVER_TEMP_AND_STALLED           ServoError = 5
	OVER_VOLT_AND_STALLED           ServoError = 6
	OVER_TEMP_OVER_VOLT_AND_STALLED ServoError = 7
)

type Servo struct {
	ID       uint8
	network  *Network
	MinAngle int
	MaxAngle int
	//For use as an estimated position for quick lookup without querying the servo
	lastSentAngle int
	//Not sure what this would be for but its cheap to hold onto for now
	lastReadAngle int
}

func clamp(min, max, input int) int {
	if input < min {
		return min
	} else if input > max {
		return max
	}
	return input
}

func clampFloat(min, max, input float32) float32 {
	if input < min {
		return min
	} else if input > max {
		return max
	}
	return input
}

// Celsius to Fahrenheit
func CToF(celsius int) int {
	return celsius/5*9 + 32
}

// Fahrenheit to Celsius
func FToC(fahrenheit int) int {
	return (fahrenheit - 32) * 5 / 9
}

func mapValue(x float32, inMin float32, inMax float32, outMin int, outMax int) int {
	return int((x-inMin)*float32(outMax-outMin)/(inMax-inMin) + float32(outMin))
}

func mapValueFloat(x int, inMin int, inMax int, outMin float32, outMax float32) float32 {
	return (float32(x)-float32(inMin))*(outMax-outMin)/(float32(inMax)-float32(inMin)) + outMin
}

func lowerByte(value uint16) uint8 {
	val := value % 256
	return uint8(val)
}

func higherByte(value uint16) uint8 {
	return uint8(value >> 8)
}

func combineBytes(lower, higher uint8) uint16 {
	return (uint16(higher) << 8) | uint16(lower)
}

func intToUint(val int16) uint16 {
	var newVal uint16
	if val < 32767 {
		tempVal := int(val) + 65536
		newVal = uint16(tempVal)
	} else {
		newVal = uint16(val)
	}
	return newVal
}

func uintToInt(val uint16) int16 {
	var newVal int16
	if val > 32767 {
		tempVal := int(val) - 65536
		newVal = int16(tempVal)
	} else {
		newVal = int16(val)
	}
	return newVal
}

func NewServo(network *Network, id uint8) *Servo {
	return &Servo{
		ID:       id,
		MinAngle: 0,
		MaxAngle: 240,
		network:  network,
	}
}

func (servo *Servo) SetMinAngle(angle int) {
	servo.MinAngle = angle
}

func (servo *Servo) SetMaxAngle(angle int) {
	servo.MaxAngle = angle
}

func (servo *Servo) SetNetwork(network *Network) {
	servo.network = network
}

// Move the servo to the specified angle over the duration of time provided
func (servo *Servo) TimedMove(angle float32, duration uint16) error {
	duration = uint16(clamp(0, 30000, int(duration)))
	angle = clampFloat(0, 240, angle)

	mappedAngle := uint16(mapValue(angle, 0, 240, 0, 1000))
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_TIME_WRITE,
		[]byte{
			lowerByte(mappedAngle),
			higherByte(mappedAngle),
			lowerByte(duration),
			higherByte(duration),
		},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) TimedMoveRead() (angle float32, duration uint16, err error) {
	/*
		Parameter 1: lower 8 bits of angle value
		Parameter 2: higher 8 bits of angle, range 0~1000
		Parameter 3: lower 8 bits of time value
		Parameter 4: higher 8 bits of time value, range 0~30000ms
	*/
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_TIME_READ,
		[]byte{},
	)
	_, err = servo.network.Write(packet)
	if err != nil {
		return 0, 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 4 {
		return 0, 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 4, len(payload))
	}

	mappedAngle := mapValueFloat(int(combineBytes(payload[0], payload[1])), 0, 1000, 0, 240)

	return mappedAngle, combineBytes(payload[2], payload[3]), nil

}

func (servo *Servo) TimedMoveWait(angle float32, duration uint16) error {
	duration = uint16(clamp(0, 30000, int(duration)))
	angle = clampFloat(0, 240, angle)

	mappedAngle := uint16(mapValue(angle, 0, 240, 0, 1000))
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_TIME_WAIT_WRITE,
		[]byte{
			lowerByte(mappedAngle),
			higherByte(mappedAngle),
			lowerByte(duration),
			higherByte(duration),
		},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) TimedMoveWaitRead() (angle float32, duration uint16, err error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_TIME_WAIT_READ,
		[]byte{},
	)
	_, err = servo.network.Write(packet)
	if err != nil {
		return 0, 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 4 {
		return 0, 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 4, len(payload))
	}

	mappedAngle := mapValueFloat(int(combineBytes(payload[0], payload[1])), 0, 1000, 0, 240)

	return mappedAngle, combineBytes(payload[2], payload[3]), nil
}

func (servo *Servo) MoveStart() error {
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_START,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) MoveStop() error {
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_STOP,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) Stop() error {
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_STOP,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) SetID(id uint8) error {
	if id != uint8(clamp(1, 253, int(id))) {
		return errors.New("cannot set id out of range")
	}

	packet := CreatePacket(
		servo.ID,
		SERVO_ID_WRITE,
		[]byte{id},
	)
	_, err := servo.network.Write(packet)
	if err == nil {
		servo.ID = id
	}
	return err
}

func (servo *Servo) GetID() (uint8, error) {
	packet := CreatePacket(
		BroadcastIdent,
		SERVO_ID_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, err
	}
	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if len(payload) != 1 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}
	if err != nil && payload[0] == 0 {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}
	return payload[0], nil
}

/*
servo deviation, range -125~ 125, The corresponding angle of
-30 ° ~ 30 °, when this command reach to the servo, the servo will immediately
rotate to adjust the deviation.
Note 1:The adjusted deviation value is not saved when powered down. To save offset use SaveOffset instead
Note 2: Because the parameter is “signed char” type of data, and the
command packets to be sent are “unsigned char” type of data, so before
sending, parameters are forcibly converted to “unsigned char” data and then
put them in command packet.
*/
func (servo *Servo) SetOffset(angle float32) error {
	angle = clampFloat(-30, 30, angle)

	mappedDeviation := mapValue(angle, 0, 30, -125, 125)
	if mappedDeviation < 0 {
		mappedDeviation += 256
	}

	packet := CreatePacket(
		servo.ID,
		SERVO_ANGLE_OFFSET_ADJUST,
		[]byte{uint8(mappedDeviation)},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) SaveOffset() error {
	packet := CreatePacket(
		servo.ID,
		SERVO_ANGLE_OFFSET_WRITE,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) GetOffset() (angle float32, err error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_ANGLE_OFFSET_READ,
		[]byte{},
	)
	_, err = servo.network.Write(packet)
	if err != nil {
		return 0, err
	}
	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}
	if len(payload) != 1 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}

	/*	deviation := payload[0]
		if deviation > 127 {
			deviation -= 127
		}
	*/
	mappedDeviation := mapValueFloat(int(payload[0]), -125, 125, -30, 30)

	return mappedDeviation, nil
}

/*
Parameter 1: lower 8 bits of minimum angleParameter 2: higher 8 bits of minimum angle, range 0~1000
Parameter 3: lower 8 bits of maximum angle
Parameter 4: higher 8 bits of maximum angle, range 0~1000
And the minimum angle value should always be less than the maximum angle
value. The command is sent to the servo, and the rotation angle of the servo
will be limited between the minimum and maximum angle. And the angle limit
value supports ‘power-down save’.
*/
func (servo *Servo) SetAngleLimit(minAngle float32, maxAngle float32) error {
	if maxAngle < minAngle {
		minAngle, maxAngle = maxAngle, minAngle
	}

	minAngle = clampFloat(0, 240, minAngle)
	maxAngle = clampFloat(0, 240, maxAngle)

	mappedMin := uint16(mapValue(minAngle, 0, 240, 0, 1000))
	mappedMax := uint16(mapValue(maxAngle, 0, 240, 0, 1000))
	packet := CreatePacket(
		servo.ID,
		SERVO_MOVE_TIME_WAIT_WRITE,
		[]byte{
			lowerByte(mappedMin),
			higherByte(mappedMin),
			lowerByte(mappedMax),
			higherByte(mappedMax),
		},
	)
	_, err := servo.network.Write(packet)
	return err

}

func (servo *Servo) GetAngleLimit() error {
	return errors.New("GetAngleLimit() not implemented")
}

/*
Parameter 1: lower 8 bits of minimum input voltage
Parameter 2: higher 8 bits of minimum input voltage, range 4500~12000mv
Parameter 3: lower 8 bits of maximum input voltage
Parameter 4: higher 8 bits of maximum input voltage, range 4500~12000mv
And the minimum input voltage should always be less than the maximum input
voltage. The command is sent to the servo, and the input voltage of the servo
will be limited between the minimum and the maximum. If the servo is out of
range, the led will flash and alarm (if an LED alarm is set). In order to protect
the servo, the motor will be in the unloaded power situation, and the servo will
not output torque and the input limited voltage value supports for power-down
save
*/
func (servo *Servo) SetVoltageLimits(minVoltage uint16, maxVoltage uint16) error {
	if maxVoltage < minVoltage {
		maxVoltage, minVoltage = minVoltage, maxVoltage
	}

	minVoltage = uint16(clamp(4500, 12000, int(minVoltage)))
	maxVoltage = uint16(clamp(4500, 12000, int(maxVoltage)))

	packet := CreatePacket(
		servo.ID,
		SERVO_VIN_LIMIT_WRITE,
		[]byte{
			lowerByte(minVoltage),
			higherByte(minVoltage),
			lowerByte(maxVoltage),
			higherByte(maxVoltage),
		},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) GetVoltageLimits() (int, int, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_VIN_LIMIT_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 4 {
		return 0, 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 4, len(payload))
	}

	return int(combineBytes(payload[0], payload[1])), int(combineBytes(payload[2], payload[3])), nil
}

/*
Parameter 1: The maximum temperature limit inside the servo range
50~100°C, the default value is 85°C, if the internal temperature of the servo
exceeds this value the led will flash and alarm (if an LED alarm is set). In order
to protect the servo, the motor will be in the unloaded power situation, and the
servo will not output torque until the temperature below this value of the servo,
then it will once again enter the working state.and this value supports for
power-down save.
*/
func (servo *Servo) SetMaxTemp(temp int) error {
	temp = clamp(50, 100, temp)

	packet := CreatePacket(
		servo.ID,
		SERVO_TEMP_MAX_LIMIT_WRITE,
		[]byte{uint8(temp)},
	)
	_, err := servo.network.Write(packet)
	return err
}

func (servo *Servo) GetMaxTemp() (int, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_TEMP_MAX_LIMIT_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 1 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}

	return int(payload[0]), nil
}

func (servo *Servo) GetTemp() (int, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_TEMP_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 1 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}

	return int(payload[0]), nil
}

func (servo *Servo) GetVoltage() (int, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_VIN_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 2 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 2, len(payload))
	}

	return int(combineBytes(payload[0], payload[1])), nil
}

/*
Returned the angular position value need to be converted to signed short int type of data, because the read angle may be negative
*/
func (servo *Servo) GetAngle() (float32, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_POS_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 2 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 2, len(payload))
	}

	//	fmt.Println("dumping payload: ", payload, " combined Bytes: ", combineBytes(payload[0], payload[1]))
	return float32(uintToInt(combineBytes(payload[0], payload[1]))) * 0.24, nil
}

/*
Parameter 1: Servo mode, range 0 or 1, 0 for position control mode, 1 for
motor control mode, default 0,
Parameter 2: null value
Parameter 3: lower 8 bits of rotation speed value
Parameter 4: higher 8 bits of rotation speed value. range -1000~1000,
Only in the motor control mode is valid, control the motor speed, the value of
the negative value represents the reverse, positive value represents the
forward rotation. Write mode and speed do not support power-down save.
Note: Since the rotation speed is the “signed short int” type of data, it is forced
to convert the data to “unsigned short int “type of data before sending the
command packet.
*/
func (servo *Servo) SetMode(mode ServoMode, rotationSpeed int16) error {
	if mode > 1 {
		return errors.New("requested invalid mode")
	}

	rotationSpeed = int16(clamp(-1000, 1000, int(rotationSpeed)))

	convertedVal := intToUint(rotationSpeed)

	packet := CreatePacket(
		servo.ID,
		SERVO_OR_MOTOR_MODE_WRITE,
		[]byte{
			uint8(mode),
			0,
			lowerByte(convertedVal),
			higherByte(convertedVal),
		},
	)
	_, err := servo.network.Write(packet)
	return err

}

func (servo *Servo) GetMode() (mode uint8, rotationSpeed int16, err error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_OR_MOTOR_MODE_READ,
		[]byte{},
	)
	_, err = servo.network.Write(packet)
	if err != nil {
		return 0, 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 4 {
		return 0, 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 4, len(payload))
	}

	return payload[0], uintToInt(combineBytes(payload[2], payload[3])), nil
}

/*
Parameter 1: Whether the internal motor of the servo is unloaded power-down
or not, the range 0 or 1, 0 represents the unloading power down, and the servo
has no torque output. 1 represents the loaded motor, then the servo has a
torque output, the default value is 0.
*/
func (servo *Servo) MotorEnable(enabled bool) error {
	var val uint8
	if enabled {
		val = 1
	} else {
		val = 0
	}
	packet := CreatePacket(
		servo.ID,
		SERVO_LOAD_OR_UNLOAD_WRITE,
		[]byte{val},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return err
	}
	return nil
}

func (servo *Servo) MotorEnabled() (bool, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_LOAD_OR_UNLOAD_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return false, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return false, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 1 {
		return false, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}
	if payload[0] > 1 {
		return false, fmt.Errorf("motor state out of range %d", payload[0])
	}

	if payload[0] == 0 {
		return false, nil
	}
	return true, nil
}

/*
Parameter 1: LED light/off state, the range 0 or 1, 0 represents that the LED is
always on. 1 represents the LED off, the default 0, and support power-down
save
*/
func (servo *Servo) LedEnable(enabled bool) error {
	var state uint8
	if enabled {
		state = 0
	} else {
		state = 1
	}

	packet := CreatePacket(
		servo.ID,
		SERVO_LED_CTRL_WRITE,
		[]byte{state},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return err
	}
	return nil
}

func (servo *Servo) LedEnabled() (bool, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_LED_CTRL_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return false, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return false, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 1 {
		return false, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}

	var state bool
	if payload[0] == 0 {
		state = true
	} else {
		state = false
	}

	return state, nil
}

/*
Parameter 1: what faults will cause LED flashing alarm value, range 0~7
There are three types of faults that cause the LED to flash and alarm,
regardless of whether the LED is in or off. The first fault is that internal
temperature of the servo exceeds the maximum temperature limit (this value is
set at point 16). The second fault is that the servo input voltage exceeds the
limit value (this value is set at 14 points). The third one is
when locked-rotor occurred.
. This value corresponds to the fault alarm relationship as shown below:
   |---|---------------------------------------------|
   | 0 | No alarm                                    |
   | 1 | Over temperature                            |
   | 2 | Over voltage                                |
   | 3 | Over temperature and over voltage           |
   | 4 | Locked-rotor                                |
   | 5 | Over temperature and stalled                |
   | 6 | Over voltage and stalled                    |
   | 7 | Over temperature, over voltage, and stalled |
   |---|---------------------------------------------|
*/

func (servo *Servo) SetLedErrorMode(mode ServoError) error {
	mode = ServoError(clamp(0, 7, int(mode)))
	packet := CreatePacket(
		servo.ID,
		SERVO_LED_ERROR_WRITE,
		[]byte{uint8(mode)},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return err
	}
	return nil
}

func (servo *Servo) GetLedErrorMode() (ServoError, error) {
	packet := CreatePacket(
		servo.ID,
		SERVO_LED_ERROR_READ,
		[]byte{},
	)
	_, err := servo.network.Write(packet)
	if err != nil {
		return 0, err
	}

	var payload []byte
	payload, err = servo.network.Read(servo.ID)
	if err != nil {
		return 0, fmt.Errorf("failed to read packet from servo: %s", err)
	}

	if len(payload) != 1 {
		return 0, fmt.Errorf("recieved invalid reply length. expected %d, recieved %d", 1, len(payload))
	}

	return ServoError(payload[0]), nil
}
