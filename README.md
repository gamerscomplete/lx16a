LX-16A GoLang Library
This is a GoLang library for controlling LX-16A serial bus servos. The library includes functionality for initializing, configuring, and controlling LX-16A servos via a serial port.

Installation
To install the library, simply run:

sh
Copy code
go get gitlab.com/gamerscomplete/lx16a
Usage
To use the library, import the package into your GoLang code:

go
Copy code
import "gitlab.com/gamerscomplete/lx16a"
Initializing the Servo
To initialize a new LX-16A servo, create a new instance of the lx16a.Servo struct:

go
Copy code
servo := lx16a.Servo{
    ID:        1,
    BaudRate:  115200,
    SerialDev: "/dev/ttyUSB0",
}
Controlling the Servo
To control the servo, use the SetPosition method:

go
Copy code
position := 512 // Set the position to 512 (midpoint)
speed := 100     // Set the speed to 100
servo.SetPosition(position, speed)
Cleaning Up
To clean up and close the serial port, use the Close method:

go
Copy code
servo.Close()
Contributing
Contributions are welcome! If you'd like to contribute to the project, please follow these steps:

Fork the repository
Create a new branch (git checkout -b my-new-feature)
Make your changes and commit them (git commit -am 'Add new feature')
Push to the branch (git push origin my-new-feature)
Create a new pull request
License
This project is licensed under the MIT License - see the LICENSE.md file for details.
